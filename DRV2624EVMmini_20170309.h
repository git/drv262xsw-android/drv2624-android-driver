/*
** =============================================================================
** Created at 4:58:12 PM
** Target : DRV2624EVMmini
** Please be noted that the data begin at address 0x0000.
** =============================================================================
*/

const static unsigned char wavedata[25]={
0x00, /* version byte */
0x0, 0x7, 0x4, /* header for waveform 0*/
0x0, 0xb, 0xe, /* header for waveform 1*/
0x3f, 0x9, 0x41, 0x5, 0x3f, 0x4, 0x13, 0x6, 0x41, 0x3, 0x0, 0xc, 0x3f, 0x4, 0x13, 0x6, 
0x41, 0x3
};

